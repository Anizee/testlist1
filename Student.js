import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function Student() {
  const [studentState, setStudentState] = useState([]);

  useEffect(() => {
    let studentState = [
      { id: 1, movie:"how you like that" , color:"red" },
      { id: 2, movie:"love sick" , color:"pink"},
      { id: 3, movie:"sour candy" ,color:"black"},
      { id: 3, movie:"love me" ,color:"yellow"}
    ];

    setStudentState(
      studentState.map(d => {
        return {
          select: false,
          id: d.id,
          movie: d.movie,
          color: d.color
        };
      })
    );
  }, []);

  return (
    <div className="container">
      <Link to="/add">
       
        <button
          type="button"
          className="btn btn-primary btn-sm float-right my-3">
          Delete
        </button>
         <button
          type="button"
          className="btn btn-primary btn-sm float-right my-3">
          Add
        </button>
      </Link>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th scope="col">
              <input
                type="checkbox"
                onChange={e => {
                  let checked = e.target.checked;
                  setStudentState(
                    studentState.map(d => {
                      d.select = checked;
                      return d;
                    })
                  );
                }}
              ></input>
            </th>
            <th scope="col">Movie</th>
            <th scope="col">Color</th>
            </tr>
        </thead>
        <tbody>
          {studentState.map((d, i) => (
            <tr key={d.id}>
              <th scope="row">
                <input
                  onChange={event => {
                    let checked = event.target.checked;
                    setStudentState(
                      studentState.map(data => {
                        if (d.id === data.id) {
                          data.select = checked;
                        }
                        return data;
                      })
                    );
                  }}
                  type="checkbox"
                  checked={d.select}
                ></input>
              </th>
              <td>{d.movie}</td>
              <td>{d.color}</td>
              </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Student;
