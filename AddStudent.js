import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";

function AddStudent(props) {
  const onSave = e => {
    let movie = e.target[0].value;
    let color = e.target[1].value;
    let data = {
      movie,
      color
    };
    addStudent(data);
  };

  const addStudent = data => {
    axios
      .post("http://localhost:8080/students", data)
      .then(d => {
        console.log(d);
        props.history.push("/");
      })
      .catch(er => alert(er));
  };
  return (
    <div className="container my-3">
      <form
        onSubmit={e => {
          e.preventDefault();
          onSave(e);
        }}
      >
        <div className="form-group">
          <label>Movie</label>
          <input type="text" className="form-control" required />
        </div>
        <div className="form-group">
          <label>Color</label>
          <input type="text" className="form-control" required />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}


export default withRouter(AddStudent);
